// console.log(`Hello World!`);

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function personalData() {
  const fullName = prompt(`Please enter your full name: `);
  const age = prompt(`Please enter your current age: `);
  const location = prompt(`Please enter your current address: `);

  console.log(`Hello, ${fullName}.`);
  console.log(`You are ${age} years old.`);
  console.log(`You live in ${location}.`);
}
personalData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function favoriteBands() {
  const band1 = `Paramore`;
  const band2 = `Dashboard Confessional`;
  const band3 = `KISS`;
  const band4 = `A Rocket to the Moon`;
  const band5 = `The Beatles `;
  console.log(`1. ${band1}`);
  console.log(`2. ${band2}`);
  console.log(`3. ${band3}`);
  console.log(`4. ${band4}`);
  console.log(`5. ${band5}`);
}
favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
function favoriteMovies() {
  const movie1 = `Bounty Hunter`;
  const movie2 = `Captain America: Civil War`;
  const movie3 = `Small Soldiers`;
  const movie4 = `The Benchwarmers`;
  const movie5 = `Dennis the Menace`;
  console.log(`1. ${movie1}`);
  console.log(`Rotten Tomatoes Rating: 12%`);
  console.log(`2. ${movie2}`);
  console.log(`Rotten Tomatoes Rating: 90%`);
  console.log(`3. ${movie3}`);
  console.log(`Rotten Tomatoes Rating: 49%`);
  console.log(`4. ${movie4}`);
  console.log(`Rotten Tomatoes Rating: 13%`);
  console.log(`5. ${movie5}`);
  console.log(`Rotten Tomatoes Rating: 27%`);
}
favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function () {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();
